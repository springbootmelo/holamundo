package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.UsuarioEntity;

public interface UsuarioService {
	List<UsuarioEntity> getUsuarios();

	UsuarioEntity getUsuario(Integer id);

	UsuarioEntity createUsuario(UsuarioEntity entity);

	UsuarioEntity updateUsuario(UsuarioEntity entity);

	void deleteUsuario(Integer id);

	List<UsuarioEntity> getUsuariosAdmin();

	UsuarioEntity getUsuariosUser(String email, String password);
}
