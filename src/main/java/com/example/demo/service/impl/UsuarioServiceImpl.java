package com.example.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.UsuarioEntity;
import com.example.demo.repository.UsuarioRepository;
import com.example.demo.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public List<UsuarioEntity> getUsuarios() {
		return usuarioRepository.findAll();
	}

	@Override
	public UsuarioEntity getUsuario(Integer id) {
		return usuarioRepository.findById(id).orElse(null);
	}

	@Override
	public UsuarioEntity createUsuario(UsuarioEntity entity) {
		if (!usuarioRepository.existsById(entity.getId())) { // Es lo mismo que usuarioRepo... == false
			return usuarioRepository.save(entity);
		} else {
			throw new RuntimeException("Ya existe el usuario");
		}
	}

	@Override
	public UsuarioEntity updateUsuario(UsuarioEntity entity) {
		if (usuarioRepository.existsById(entity.getId())) { // Comprueba que existe
			return usuarioRepository.save(entity);
		} else {
			throw new RuntimeException("No existe el usuario");
		}
	}

	@Override
	public void deleteUsuario(Integer id) {
		usuarioRepository.deleteById(id);
	}
	
	@Override
	public List<UsuarioEntity> getUsuariosAdmin(){ // PARA DEVOLVER UNA LISTA
		return usuarioRepository.getUsuariosAdmin();
	}
	
	@Override
	public UsuarioEntity getUsuariosUser(String email, String password) { //PARA DEVOLVER UN VALOR
		return usuarioRepository.findByEmailAndPassword(email, password);
	}
}
