package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.UsuarioEntity;

@Repository

public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Integer> {
//	@Query("SELECT u FROM UsuarioEntity u WHERE perfil.id =1") CONSULTA EN HQL
	@Query(value = "select * from usuario u where idperfil = 1 ", nativeQuery = true)
	// PARA CONSULTA ESPECIFICA DEL SGBD
	List<UsuarioEntity> getUsuariosAdmin();

	// @Query(value = "select u from UsuarioEntity u where email = :email and
	// password = :password ") SI NO ES SENCILLO
	UsuarioEntity findByEmailAndPassword(String email, String password);
	//QUERY METHOD DE SPRING BOOT SI ES SENCILLO EL QUERY
}
