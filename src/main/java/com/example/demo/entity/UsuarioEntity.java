package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "usuario")
@Data
public class UsuarioEntity {
	@Id
	private Integer id;
	private String nombre;
	private String apellidop;
	private String password;
	private String email;
	@ManyToOne 
	@JoinColumn(name = "idperfil")
	private PerfilEntity perfil;
}
