package com.example.demo.controller;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AdvisorController {

	@ExceptionHandler({ EmptyResultDataAccessException.class})
	public String handleException() {
		//ATRAPA LA EXCEPCION ORIGINAL (EXCEPTION HANDLER),Y LA REEMPLAZA POR UNA PERSONALIZADA (handleException)
		return "No se encontro el registro";
	}
	
}
