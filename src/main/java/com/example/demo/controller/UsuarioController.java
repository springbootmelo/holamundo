package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.UsuarioEntity;
import com.example.demo.service.UsuarioService;

@RestController

public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("usuarios")
	public List<UsuarioEntity> getUsuarios() {
		return usuarioService.getUsuarios();
	}

	@GetMapping("usuarios/{id}")
	public UsuarioEntity getUsuario(@PathVariable Integer id) {
		return usuarioService.getUsuario(id);
	}

	@PostMapping("usuarios")
	public UsuarioEntity createUsuario(@RequestBody UsuarioEntity entity) {
		return usuarioService.createUsuario(entity);
	}

	@PutMapping("usuarios")
	public UsuarioEntity updateUsuario(@RequestBody UsuarioEntity entity) {
		return usuarioService.updateUsuario(entity);
	}

	@DeleteMapping("usuarios/{id}")
	public void deleteUsuario(@PathVariable Integer id) {
		usuarioService.deleteUsuario(id);
	}

	@GetMapping("usuarios/admin")
	public List<UsuarioEntity> getUsuariosAdmin() {
		return usuarioService.getUsuariosAdmin();
	}

	@GetMapping("usuarios/{email}/{password}")
	public UsuarioEntity getUsuariosUser(@PathVariable String email, @PathVariable String password) {
		return usuarioService.getUsuariosUser(email, password);
	}

}
